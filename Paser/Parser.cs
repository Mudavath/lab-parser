﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Paser
{
    public class Parser
    {
        private readonly string Path;
        private List<LaboratoryTestResul> ResultTests;
        public Parser(string path)
        {
            Path = path;
            ResultTests = new List<LaboratoryTestResul>();
        }
        public void mapped_results()
        {
            // Read file using StreamReader. Reads file line by line    
            using (StreamReader file = new StreamReader(Path))
            {
                int counter = 0;
                string ln;
                int lastOBX = 0;

                while ((ln = file.ReadLine()) != null)
                {

                    LaboratoryTestResul test = new LaboratoryTestResul();
                    var columns = ln.Split('|');

                    if (columns[0].ToString() == Const.OBX)
                    {
                        lastOBX = int.Parse(columns[1]);
                        test.LineTye = Const.OBX;
                        switch (columns[2].ToString())
                        {
                            case "C100":
                                test.Result = float.Parse(columns[3].ToString());
                                test.Format = Const.FLOAT;
                                test.Code = Const.C100;
                                break;

                            case "C200":
                                test.Result = float.Parse(columns[3].ToString());
                                test.Format = Const.FLOAT;
                                test.Code = Const.C200;
                                break;

                            case "A250":
                                if (columns[3].Equals(Const.BOOLEAN_NEGATIVE))
                                {
                                    test.Result = float.Parse(Const.BOOLEAN_NEGATIVE_MAPPED.ToString());
                                }
                                else
                                {
                                    test.Result = float.Parse(Const.BOOLEAN_POSITIVE_MAPPED.ToString());
                                }
                                test.Format = Const.BOOLEAN;
                                test.Code = Const.B250;
                                break;

                            case "B250":
                                if (columns[3].ToString() == Const.NIL_3PLUS_NIL)
                                {
                                    test.Result = float.Parse(Const.NIL_3PLUS_NIL_FORMAT_MAPPED.ToString());
                                }
                                else if (columns[3].ToString() == Const.NIL_3PLUS_PLUS)
                                {
                                    test.Result = float.Parse(Const.NIL_3PLUS_PLUS_FORMAT_MAPPED.ToString());
                                }
                                else if (columns[3].ToString() == Const.NIL_3PLUS_2PLUS)
                                {
                                    test.Result = float.Parse(Const.NIL_3PLUS_2PLUS_FORMAT_MAPPED.ToString());
                                }
                                else
                                {
                                    test.Result = float.Parse(Const.NIL_3PLUS_3PLUS_MAPPED.ToString());
                                }
                                test.Format = Const.BOOLEAN;
                                test.Code = Const.B250;
                                break;

                            default:
                                break;
                        }
                        ResultTests.Add(test);
                    }
                    else
                    {
                        var lastTest = ResultTests[lastOBX -1];

                        if (string.IsNullOrEmpty(lastTest.Comment))
                        {
                            lastTest.Comment = columns[2].ToString();
                        }
                        else
                        {
                            lastTest.Comment += $"\n{columns[2].ToString()} ";
                        }
                    }
                    counter++;
                }
                file.Close();
                foreach (var item in ResultTests)
                {
                    Console.WriteLine(
                       $"code: {item.Code}, " +
                       $"result: {item.Result}, " +
                       $"format:{ item.Format}, " +
                       $"comment:{item.Comment}"
                   );
                }
                Console.WriteLine($"File has {counter} lines.");
                Console.ReadKey();
            }
        }
    }
}
