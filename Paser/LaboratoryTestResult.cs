﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paser
{
    public class LaboratoryTestResul
    {
        public string Code { get; set; }
        public float Result { get; set; }
        public string Format { get; set; }
        public string Comment { get; set; }
        public string LineTye { get; set; }
    }

    public static class Const
    {
        public static string OBX = "OBX";
        public static string NET = "NTE";
        public static string C100 = "C100";
        public static string C200 = "C200";
        public static string A250 = "A250";
        public static string B250 = "B250";

        public static string FLOAT = "FLOAT";

        //Inputs
        public static string BOOLEAN_NEGATIVE = "NEGATIVE";
        public static string BOOLEAN_POSITIVE = "POSITIVE";

        public static string NIL_3PLUS_NIL = "NIL";
        public static string NIL_3PLUS_PLUS = "+";
        public static string NIL_3PLUS_2PLUS = "++";
        public static string NIL_3PLUS_3PLUS = "+++";


        //Formats
        public static string BOOLEAN = "boolean";
        public static string NIL_3PLUS_NIL_FORMAT = "nil_3plus";


        //Mapped Values
        public static int BOOLEAN_NEGATIVE_MAPPED = -1;
        public static int BOOLEAN_POSITIVE_MAPPED = -2;

        public static int NIL_3PLUS_NIL_FORMAT_MAPPED = -1;
        public static int NIL_3PLUS_PLUS_FORMAT_MAPPED = -2;
        public static int NIL_3PLUS_2PLUS_FORMAT_MAPPED = -2;
        public static int NIL_3PLUS_3PLUS_MAPPED =  -3;

    }
}
